# Front End Challenge

- the files inside "react" folder represent the work I did on this challenge over the course of several hours.
- instead of using a drop down to select search by representative or senator I opted to make a toggle.
- I did not complete all styling as directed due to time constraints, however I did undertake basic styling. See screenshot for preview.
- Focus was on functionality

- to run locally, first download all dependencies and then run "node index.js", then cd into react directory and "yarn run start"

[Link to screenshot of app](https://imgur.com/a/3FIWMsU)
