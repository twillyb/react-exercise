import LineItem from "./LineItem";
import DisplayHeader from "./DisplayHeader";
import DisplayDetails from "./DisplayDetails";
import React, { useState, useEffect } from "react";

const DisplayResults = ({ results, isHidden }) => {
  const [relevantPerson, setRelevantPerson] = useState(null);
  const [isDeetsHidden, setIsDeetsHidden] = useState(true);

  useEffect(() => {
    setRelevantPerson(null);
    setIsDeetsHidden(true);
  }, [isHidden]);

  let handleClick = (person) => {
    setRelevantPerson(person);
    setIsDeetsHidden(false);
  };

  if (isHidden) {
    return null;
  } else {
    return (
      <div>
        <div className="DisplayResults">
          <DisplayHeader />

          {results.map((i) => {
            return (
              <div key={i.link} onClick={() => handleClick(i)}>
                <LineItem personObject={i} />
              </div>
            );
          })}
        </div>
        <DisplayDetails
          isDeetsHidden={isDeetsHidden}
          personObj={relevantPerson}
        />
      </div>
    );
  }
};
export default DisplayResults;
