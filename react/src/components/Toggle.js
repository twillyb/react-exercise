const Toggle = ({ currentSelection, handleClick }) => {
  let left = "selected";
  let right = "notSelected";

  if (currentSelection !== "representatives") {
    left = "notSelected";
    right = "selected";
  } else {
    left = "selected";
    right = "notSelected";
  }

  return (
    <div>
      <div> Search by:</div>
      <div className={left} onClick={handleClick}>
        Representative
      </div>
      <div className={right} onClick={handleClick}>
        Senator
      </div>
    </div>
  );
};

export default Toggle;
