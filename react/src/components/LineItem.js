const LineItem = ({ personObject }) => {
  return (
    <div className="lineItem">
      <span>{personObject.name}</span>
      <span>{personObject.party}</span>
    </div>
  );
};

export default LineItem;
