const DisplayDetails = ({ personObj, isDeetsHidden }) => {
  let district = personObj ? "District: " + personObj.district : "District";
  let firstName = personObj
    ? "First Name: " + personObj.name.split(" ")[0]
    : "First Name";
  let lastName = personObj
    ? "Last Name: " + personObj.name.split(" ")[1]
    : "Last Name";
  let phone = personObj ? "Phone: " + personObj.phone : "Phone";

  let office = personObj ? "Office: " + personObj.office : "Office";

  let website = personObj ? <a href={personObj.link}>Website</a> : "Website";

  if (isDeetsHidden) {
    return null;
  } else {
    return (
      <div>
        <h4>Info</h4>
        <div>{firstName}</div>
        <div>{lastName}</div>
        <div>{district}</div>
        <div>{phone}</div>
        <div>{office}</div>
        <div>{website}</div>
      </div>
    );
  }
};

export default DisplayDetails;
