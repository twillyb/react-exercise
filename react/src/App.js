import "./App.css";
import React, { useState } from "react";

import SubmissionForm from "./components/SubmissionForm.js";
import Toggle from "./components/Toggle.js";

function App() {
  const [toggle, setToggle] = useState(true);

  let currentSelection = toggle ? "representatives" : "senators";

  const handleToggle = () => {
    setToggle(!toggle);
  };

  return (
    <div className="App">
      <div className="mainFlexContainer">
        <h1 className="siteTitle"> Who's My Representative?</h1>
        <Toggle
          currentSelection={currentSelection}
          handleClick={handleToggle}
        />
        <SubmissionForm searchBy={currentSelection} />
      </div>
    </div>
  );
}

export default App;
